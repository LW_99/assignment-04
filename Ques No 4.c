#include <stdio.h>

int main()
{
    float C , F;

    printf ("Enter Temperature in Celsius :");
    scanf ("%f" , &C);

     F = (C*9/5)+32;

     printf ("Celsius in Fahrenheit: %.2f" ,F);

     return 0;
}
