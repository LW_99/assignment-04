#include <stdio.h>

int main ()
{
    int a,b;

     printf ("Enter Number One :");
     scanf ("%d" ,&a);
     printf ("Enter Number Two:");
     scanf ("%d", &b);

     //swapping

     //a = (initial_a - initial_b)
     a = a-b;

     //b = (initial_a - initial_b) +  initial_b = initial_a
     b = a+b;

     //a = initial_a - (initial_a - initial_b) = initial_b
     a = b-a;

     printf ("After Swapping Number One = %d\n" , a);
     printf ("After Swapping Number Two = %d\n" , b);

     return 0;



     }
