#include <stdio.h>

#define pi 3.14

void main ()

{
    float radius,height,volume;

    printf ("Please enter the  radius of the cone :");
    scanf ("%f" , &radius);
    printf ("Please enter the  height of the cone :");
    scanf ("%f" , &height);

    volume = (pi*radius*radius*height)/3;

    printf ("\n Volume of the cone is : %.3f", volume);
    return 0 ;
}
